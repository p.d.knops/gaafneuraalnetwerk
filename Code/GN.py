from numpy import *
from random import random, randint
from sys import argv
import matplotlib.pyplot as plt

# Returns a random featurematrix with n nodes
def random_feat(n):

    # Probability of a node having a starting label 1
    p_start = 0.5

    # Feature matrix
    feat = zeros(n)

    # Fill feat with ones with probability p_start
    for node_number in range(n):
        if random() <= p_start:
            feat[node_number] = 1

    return feat

# Returns random edges for a matrix when given an average amount of neighbours
def random_adjacency(number_of_nodes, avg_neighbours):

    n = number_of_nodes
    # Probability of two nodes being connected
    p_connect = avg_neighbours / n

    # Initialize adjacency matrix
    adj = zeros((n , n))

    # Fill the diagonal of adj with ones and the upper half with zeroes and ones
    # Mirror the upper half to the lower half to get a symmetric matrix
    for row in range(n):
        for col in range(n):
            if row == col:
                # Each node affects itself
                adj[row, col] = 1
            elif col > row:
                # Place a 1 with probability p_connected
                if random() <= p_connect:
                    adj[row ,col] = 1
            else:
                # Mirror top half of matrix to bottom half
                adj[row, col] = adj[col, row]

    """
    # We don't want 'islands' to form in our graph
    # If each 'column' in the above-diagonal part of the adjacency matrix contains at least one non-zero entry, there
    # are guaranteed to be no islands
    # In the code below we look for such all-zero above-diagonal columns
    # If we find such a column, we put a single 1 entry in a random spot in that column
    # Then we repair the symmetry of the matrix again
    # This is less than ideal, but the easiest fix I could think of
    
    # NOTE: Because of this code, avg_neighbours is not generally representative
    # of the average number of neighbors anymore. It's just a general indication
    
    # NOTE2: This method makes it so that earlier nodes have a larger average number of neighbours than later nodes
    # The symmetry in this regard is somewhat ruined
    # The effect should be relatively minor though
    """

    for col in range(1,n):
        # Check if all above-diagonal column entries are zero
        all_zero = True
        for row in range(col-1):
            if adj[row, col] != 0:
                all_zero = False

        if all_zero:
            # Pick entry in column to make into edge
            add_one_row = randint(0,col-1)
            add_one_col = col
            # Add edge
            adj[add_one_row,add_one_col] = 1
            # Fix symmetry
            adj[add_one_col,add_one_row] = 1

    return adj

# Updates the feature matrix by applying adjunct matrix
def network(feat, weights):

    # Gets the length of the feature matrix
    n = len(feat)

    # Need a vector of the sum of a row to normalize
    row_totals = zeros(n)


    # Start counting how many times we've updated the node values
    step = 0

    # Set a maximum of steps
    maxstep = 20

    # Initialize old_feat so we can keep track of whether node values still change
    old_feat = empty(n)
    old_feat[:] = -1

    # Boolean to keep track of whether node values still change
    feat_still_changing = True

    # Iteratively update node values based on adjacency matrix
    # Stops when the nodes aren't changing or max steps is reached
    while step < maxstep and feat_still_changing:
        # Save old values for after we update
        old_feat = feat

        # Apply adjacency matrix to update node values
        feat = dot(weights ,feat)

        # Find the sum of the rows to normalize
        for row in range(n):
            for col in range(n):
                # Add written value to row total
                row_totals[row] += weights[row, col]
            # Normalize based on weights matrix row totals
            feat[row] = feat[row] / row_totals[row]

        # Set node values to 0 or 1 depending on which results of the dot product are closer to
        # If an entry is 0.5, keep it the same as it was
        for node_number in range(n):
            if feat[node_number] < 0.5:
                feat[node_number] = 0
            elif feat[node_number] == 0.5:
                feat[node_number] = old_feat[node_number]
            else:
                feat[node_number] = 1

        # Check whether or update did anything, so we stop if it didn't
        if all(feat == old_feat):
            feat_still_changing = False

        # Update step number
        step += 1

    # Return order parameter value for final feat
    return feat

# Calculates the order parameter
# This is close to 1 if we have a lot of the same nodes
# Ans close to 0 if the nodes are well distributed
def order_parameter(feat):

    # We change every 0 to a -1
    n = len(feat)
    for i in range(n):
        if feat[i] == 0:
            feat[i] = -1

    # Returns the order parameter
    return abs(sum(feat)) / n

# Plots the results of the order parameters of different matrices
# Depending on the average amount of neighbours
def main():

    # We use a constant number of nodes of 50
    n = 50

    # We look at different values of the avg amount of neighbours
    neighbours = [x / 10 for x in range(100)]

    # Initiate vector to store order parameter values in
    order_parameters = empty(100)

    # For every number of neighbours, we calculate the order parameter
    for i in range(100):
        neighbour = neighbours[i]
        order_parameter_avg = 0
        # We use 500 calculations to get a representative value
        for j in range(50):
            matrix = random_feat(n)

            adj = random_adjacency(n, neighbour)
            weights = zeros((n,n))
            for row in range(n):
                for col in range(n):
                    if adj[row,col] == 1:
                        # Initiate weights randomly between -1 and 1, except on diagonal where we put it to 2
                        if row == col:
                            weights[row, col] = 2
                        else:
                            weights[row,col] = random()*2-1


            order_parameter_avg += order_parameter(network(matrix, weights))
        order_parameter_avg = order_parameter_avg / 50
        order_parameters[i] = order_parameter_avg
        print("The order parameter for a matrix with an average of ", neighbour ,"neighbours is ", order_parameter_avg)

    # Plot our results
    plt.scatter(neighbours, order_parameters)
    plt.show()

main()