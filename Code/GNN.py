import numpy as np
import random as rnd
from math import sqrt


# Returns a random featurematrix with n nodes
def random_feat(n):

    # Probability of a node having a starting label 1
    p_start = 0.5

    # Feature matrix
    feat = np.zeros(n)

    # Fill feat with ones with probability p_start
    for node_number in range(n):
        if rnd.random() <= p_start:
            feat[node_number] = 1
        else:
            feat[node_number] = -1

    return feat


# Returns random edges for a matrix when given an average amount of neighbours
def random_adjacency(number_of_nodes, avg_neighbours):

    n = number_of_nodes
    # Probability of two nodes being connected
    p_connect = avg_neighbours / n

    # Initialize adjacency matrix
    adj = np.zeros((n , n))

    # Initialize list that keeps track of edge positions
    # Only contains (i,j) for i>j
    edge_pos = list()

    # Fill the diagonal of adj with ones and the upper half with zeroes and ones
    # Mirror the upper half to the lower half to get a symmetric matrix
    for row in range(n):
        for col in range(n):
            if row == col:
                # Each node affects itself
                adj[row, col] = 1
            elif col > row:
                # Place a 1 with probability p_connected
                if rnd.random() <= p_connect:
                    adj[row ,col] = 1
                    edge_pos.append((row, col))
            else:
                # Mirror top half of matrix to bottom half
                adj[row, col] = adj[col, row]

    """
    # We don't want 'islands' to form in our graph
    # If each 'column' in the above-diagonal part of the adjacency matrix contains at least one non-zero entry, there
    # are guaranteed to be no islands
    # In the code below we look for such all-zero above-diagonal columns
    # If we find such a column, we put a single 1 entry in a random spot in that column
    # Then we repair the symmetry of the matrix again
    # This is less than ideal, but the easiest fix I could think of

    # NOTE: Because of this code, avg_neighbours is not generally representative
    # of the average number of neighbors anymore. It's just a general indication

    # NOTE2: This method makes it so that earlier nodes have a larger average number of neighbours than later nodes
    # The symmetry in this regard is somewhat ruined
    # The effect should be relatively minor though
    """

    for col in range(1, n):
        # Check if all above-diagonal column entries are zero
        all_zero = True
        for row in range(col - 1):
            if adj[row, col] != 0:
                all_zero = False

        if all_zero:
            # Pick entry in column to make into edge
            add_one_row = rnd.randint(0, col - 1)
            add_one_col = col
            # Add edge
            adj[add_one_row, add_one_col] = 1
            edge_pos.append((add_one_row, add_one_col))
            # Fix symmetry
            adj[add_one_col, add_one_row] = 1

    return adj, edge_pos


# Updates the feature matrix by applying adjunct matrix
def network(feat, weights):
    # Gets the length of the feature matrix
    n = len(feat)

    # Need a vector of the sum of a row to normalize
    row_totals = np.zeros(n)

    # Start counting how many times we've updated the node values
    step = 0

    # Set a maximum of steps
    maxstep = 100

    # Initialize old_feat so we can keep track of whether node values still change
    old_feat = np.empty(n)
    old_feat[:] = -1

    # Boolean to keep track of whether node values still change
    feat_still_changing = True

    # Iteratively update node values based on adjacency matrix
    # Stops when the nodes aren't changing or max steps is reached
    while step < maxstep and feat_still_changing:
        # Save old values for after we update
        old_feat = feat

        # Apply adjacency matrix to update node values
        feat = np.dot(weights, feat)

        # Find the sum of the rows to normalize
        for row in range(n):
            for col in range(n):
                # Add written value to row total
                row_totals[row] += abs(weights[row, col])
            # Normalize based on weights matrix row totals
            feat[row] = feat[row] / row_totals[row]

        # Set node values to 0 or 1 depending on which results of the dot product are closer to
        # If an entry is 0.5, keep it the same as it was
        for node_number in range(n):
            if feat[node_number] < 0:
                feat[node_number] = -1
            elif feat[node_number] == 0:
                feat[node_number] = old_feat[node_number]
            else:
                feat[node_number] = 1

        # Check whether or update did anything, so we stop if it didn't
        if all(feat == old_feat):
            feat_still_changing = False

        # Update step number
        step += 1

    return feat


def calculate_single_loss(feat, truth_feat):
    # Calculate a loss function for comparing a single feat matrix to the true feat matrix
    # The more the two differ, the higher the loss
    loss = 0
    for i in range(len(feat)):
        if feat[i] != truth_feat[i]:
            loss += 1

    return loss


def calculate_total_loss(feats, truth_feats):
    total_loss = 0

    # Calculate single losses, then sum
    # I tried quadratic sum as well, but that didn't work as well

    for i in range(len(feats)):
        feat = feats[i]
        truth_feat = truth_feats[i]
        total_loss += calculate_single_loss(feat, truth_feat)

    return total_loss


def propagate_feats_and_find_loss(weights, input_feats, truth_feats):
    # Easiest way I could find of making an array of the same dimensions
    # Would be safer to initialize as an empty array, but oh well
    output_feats = input_feats.copy()

    for i in range(len(input_feats)):
        output_feats[i] = network(input_feats[i], weights)

    return calculate_total_loss(output_feats, truth_feats)


def train():

    # Define training parameters
    trainingSteps = 1000
    changeParam = 2
    m = 100

    # Define graph characteristics
    n = 12
    avg_neighbours = 3

    # Create adjacency matrix for graph
    adj, edge_pos = random_adjacency(n, avg_neighbours)

    # Initialize weights
    # Random values in (-1,1) at edge_pos, zero elsewhere
    # Required to be symmetric, because asymmetry can a priori cause problems with network() never stopping
    weights = np.zeros((n, n))
    for edge in edge_pos:
        edge_row, edge_col = edge
        random_weight = 2 * rnd.random() - 1
        weights[edge_row, edge_col] = random_weight
        weights[edge_col, edge_row] = random_weight
    # Set weights diagonal to 1
    for i in range(n):
        weights[i, i] = 1

    # Define feat matrices we bench on
    m_check = 100
    feats_check = np.empty((m_check, n))
    for i in range(m_check):
        feats_check[i] = random_feat(n)

    # Check performance before training
    loss_on_check = propagate_feats_and_find_loss(weights, feats_check, feats_check)
    print("Loss on check set: " + str(loss_on_check))
    correct_before_training = 0
    for i in range(m_check):
        feats_check_outcome = network(feats_check[i], weights)
        if ((feats_check_outcome == feats_check[i]).all()):
            correct_before_training += 1
    print("correct before training: " + str(correct_before_training) + "/" + str(m_check))

    # Define feat matrices we train on
    feats = np.empty((m, n))
    for i in range(m):
        feats[i] = random_feat(n)

    # The model is set to train to keep these feats the same
    # (Simplest thing to do, and always guaranteed possible, by simply setting all weights zero)
    truth_feats = feats

    # Calculate initial loss
    total_loss = propagate_feats_and_find_loss(weights, feats, truth_feats)
    print(total_loss)

    # Actual training using "Local Search"
    for i in range(trainingSteps):

        # Duplicate weights matrix
        new_weights = weights.copy()

        # Select random edge whose weight to change
        edge = rnd.choice(edge_pos)
        edge_row, edge_col = edge

        # Change the weight and see if it lowers the loss (also fix symmetry of weights)
        new_weights[edge_row, edge_col] *= (2 * (rnd.random() - 0.5) * changeParam)
        new_weights[edge_col, edge_row] = new_weights[edge_row, edge_col]
        new_total_loss = propagate_feats_and_find_loss(new_weights, feats, truth_feats)

        # Only change weights if it lowers the loss
        if (new_total_loss < total_loss):
            weights = new_weights
            total_loss = new_total_loss

        # If we're there, stop changing stuff
        if (total_loss == 0):
            break

    # Check performance after training
    loss_on_check_after_training = propagate_feats_and_find_loss(weights, feats_check, feats_check)

    # Gather and print statistics
    print("Loss on check set after training: " + str(loss_on_check_after_training))
    correct_after_training = 0
    for i in range(m_check):
        feats_check_outcome = network(feats_check[i], weights)
        if ((feats_check_outcome == feats_check[i]).all()):
            correct_after_training += 1
    print("correct after training: " + str(correct_after_training) + "/" + str(m_check))

def main():
    train()


main()
